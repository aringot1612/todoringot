# TodoRingot

Projet - TP : Application Android (java)

Gestion de tâches.

## Auteur

- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçus :

### Page principale

<br>![Aperçu : Page principale](images/home.png)

### Page principale avec éléments sélectionnés

<br>![Aperçu : Page principale avec éléments sélectionnés](images/home_selected_elements.png)

### Page principale avec éléments supprimés

<br>![Aperçu : Page principale avec éléments supprimés](images/home_deleted_elements.png)

### Ajout d'une tâche

<br>![Aperçu : Ajout d'une tâche](images/dialog.png)

### Menu

<br>![Aperçu : Menu](images/menu.png)