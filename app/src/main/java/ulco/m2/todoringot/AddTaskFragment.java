package ulco.m2.todoringot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

/** Fragment permettant d'ajouter une nouvelle tâche. */
public class AddTaskFragment extends DialogFragment {
    public final static String INPUT = "ulco.m2.todoringot.INPUT";
    private Button validate;
    private Button cancel;
    private EditText input;
    ITaskData dataPasser;

    /** Création du dialogFragment. */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = createView();
        builder.setView(view);
        findViews(view);
        createEventHandlers();
        retrieveDataFromBundle(savedInstanceState);
        return builder.create();
    }

    /** Création de la vue dialog. */
    private View createView(){
        return View.inflate(getContext(), R.layout.fragment_add_task, null);
    }

    /** Récupération des éléments de layout. */
    private void findViews(View view) {
        this.validate = view.findViewById(R.id.valid);
        this.cancel = view.findViewById(R.id.cancel);
        this.input = view.findViewById(R.id.input);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        validate.setOnClickListener(v -> handleReturn(input.getText().toString()));
        cancel.setOnClickListener(v -> handleReturn(""));
    }

    /** Retour vers l'activité avec une valeur donnée en paramètre. */
    private void handleReturn(String newTask){
        passData(newTask);
        dismiss();
    }

    /** Permet de récupérer l'input dans l'activité parent. */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        dataPasser = (ITaskData) context;
    }

    public void passData(String newTask) {
        dataPasser.onTaskAdded(newTask);
    }

    /** Sauvegarde de l'input en changement d'orientation. */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(INPUT, input.getText().toString());
    }

    /** Récupération de l'input en changement d'orientation. */
    private void retrieveDataFromBundle(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            input.setText(savedInstanceState.getString(INPUT));
        }
    }
}