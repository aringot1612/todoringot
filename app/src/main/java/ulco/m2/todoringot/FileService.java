package ulco.m2.todoringot;

import android.content.Context;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/** Classe permettant de réaliser des opérations sur des fichiers. */
public class FileService {
    /** Permet d'enregistrer la todoList dans un fichier. */
    public void writeToFile(String fileName, ArrayList<String> newContent, Context context) {
        try {
            File _f = context.getFileStreamPath(fileName);
            FileWriter fw = new FileWriter(_f);
            for(String element : newContent){
                fw.write(element + "\n");
            }
            fw.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    /** Permet de récupérer la todoList à partir d'un fichier. */
    public ArrayList<String> readInFile(String fileName, Context context){
        try {
            String str;
            InputStream inputStream;
            inputStream = new FileInputStream(context.getFileStreamPath(fileName));
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            ArrayList<String> result = new ArrayList<>();
            while ((str = reader.readLine()) != null)
                result.add(str);
            inputStream.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}