package ulco.m2.todoringot;

/** Interface permettant de récupérer les valeurs du AddTaskFragment depuis l'activité principale. */
public interface ITaskData {
    void onTaskAdded(String newTask);
}