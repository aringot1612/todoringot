package ulco.m2.todoringot;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import java.util.Arrays;
import ulco.m2.todoringot.databinding.ActivityMainBinding;

/** Activité principale. */
public class MainActivity extends AppCompatActivity implements ITaskData {
    public final static String LIST_VIEW_INPUT = "ulco.m2.todoringot.LIST_VIEW_INPUT";
    public final static String LIST_VIEW_INPUT_CHECKED = "ulco.m2.todoringot.LIST_VIEW_INPUT_CHECKED";

    private ListView listView;
    private ArrayList<String> values;
    private FileService fileService;
    private ArrayAdapter<String> adapter;

    /** Création de l'activité. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);
        findViews();
        init();
        addDefaultValues();
        getSavedList();
        setAdapter();
        binding.fab.setOnClickListener(view -> toNewTaskFragment());
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        retrieveDataFromBundle(savedInstanceState);
    }

    /** Initialisation de certaines valeurs propre à l'activité. */
    private void init(){
        values = new ArrayList<>();
        this.fileService = new FileService();
    }

    /** Création du menu. */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /** Récupération des éléments de layout. */
    private void findViews(){
        this.listView = findViewById(R.id.listView);
    }

    /** Ajout de valeurs par défaut. */
    private void addDefaultValues(){
        values.addAll(Arrays.asList("Val 1", "Val 2", "Val 3", "Valeur 1", "Valeur 2", "Valeur 3", "Value 1", "Value 2", "Value 3"));
    }

    /** Suppression des tâches sélectionnées. */
    private void eraseSelected(){
        // Récupération des tâches sélectionnées.
        SparseBooleanArray checkedPositions = listView.getCheckedItemPositions();
        // Tableau permettant de récupérer les index à supprimer.
        ArrayList<Integer> toDelete = new ArrayList<>();
        for (int i = 0 ; i < listView.getCount() ; i++) {
            if(checkedPositions.get(i))
                toDelete.add(i);
        }
        // Suppression des valeurs sélectionnées.
        for(int i = toDelete.size() - 1 ; i >= 0 ; i--){
            values.remove((int)toDelete.get(i));
        }
        // Notification de changement des valeurs pour l'adapter.
        adapter.notifyDataSetChanged();
        // On vide le tableau des valeurs sélectionnées.
        listView.clearChoices();
    }

    /** Ajout d'un adapter pour nos valeurs. */
    private void setAdapter(){
        adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_checked, values);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /** Gestion des actions menu. */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.eraseSelected) eraseSelected();
        else if(item.getItemId() == R.id.saveList) saveList();
        return true;
    }

    /** Sauvegarde de la liste dans un fichier. */
    private void saveList(){
        fileService.writeToFile(getString(R.string.fileName), values, getApplicationContext());
    }

    /** Récupération de la liste depuis un fichier. */
    private void getSavedList(){
        ArrayList<String> savedList = fileService.readInFile(getString(R.string.fileName), getApplicationContext());
        if(savedList.size() > 0) values = savedList;
    }

    /** Affichage de la boite de dialogue permettant d'ajouter une tâche. */
    private void toNewTaskFragment(){
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        AddTaskFragment loginFragment = new AddTaskFragment();
        loginFragment.show(fragmentManager, getString(R.string.addTaskFragmentName));
    }

    /** Sauvegarde de différentes valeurs en changement d'orientation. */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        boolean[] toSave = new boolean[listView.getCount()];
        SparseBooleanArray checkedPositions = listView.getCheckedItemPositions();
        for (int i = 0 ; i < listView.getCount() ; i++) {
            toSave[i] = (checkedPositions.get(i));
        }
        outState.putBooleanArray(LIST_VIEW_INPUT_CHECKED, toSave);
        outState.putStringArrayList(LIST_VIEW_INPUT, values);
        super.onSaveInstanceState(outState);
    }

    /** Restauration de différentes valeurs en changement d'orientation. */
    private void retrieveDataFromBundle(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            if(!savedInstanceState.getStringArrayList(MainActivity.LIST_VIEW_INPUT).isEmpty()){
                values = savedInstanceState.getStringArrayList(MainActivity.LIST_VIEW_INPUT);
                adapter.notifyDataSetChanged();
            }
            boolean[] itemsChecked = savedInstanceState.getBooleanArray(MainActivity.LIST_VIEW_INPUT_CHECKED);
            for(int i = 0 ; i < itemsChecked.length ; i++){
                listView.setItemChecked(i, itemsChecked[i]);
            }
        }
        setAdapter();
    }

    /** Est appelée lorsqu'une valeur est retournée par la boite de dialogue. */
    @Override
    public void onTaskAdded(String newTask) {
        if (!newTask.isEmpty()) {
            values.add(newTask);
            Toast.makeText(this, getString(R.string.taskAddedHint) + newTask, Toast.LENGTH_SHORT).show();
            adapter.notifyDataSetChanged();
        }
    }
}